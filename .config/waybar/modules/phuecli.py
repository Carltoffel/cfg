#!/bin/env python

import argparse
from phue import Bridge


MINBRIGHTNESS = 1
MAXBRIGHTNESS = 254
MINTEMPERATURE = 153
MAXTEMPERATURE = 454

LAMPS = [5, 13]


parser = argparse.ArgumentParser(description="Control Philips Hue Lights")
parser.add_argument("--setb", help="set brightness")
parser.add_argument("--incb", help="increase brightness")
parser.add_argument("--decb", help="decrease brightness")
parser.add_argument("--getb", action="store_true", help="print brightness")
parser.add_argument("--sett", help="set temperature")
parser.add_argument("--inct", help="increase temperature")
parser.add_argument("--dect", help="decrease temperature")
parser.add_argument("--gett", action="store_true", help="print temperature")
parser.add_argument("--state", action="store_true", help="print status")
parser.add_argument("-t", "--toggle", action="store_true", help="toggle lamp")
parser.add_argument("-l", "--lamp", help="select lamp")

args = parser.parse_args()

b = Bridge("<put-bridge-ip-here>")


def percental_value(value, min_value, max_value):
    value = int(min_value + value/100 * (max_value-min_value + 0.5))
    return value


def trim_value(value, min_value, max_value):
    value = max(min_value, min(max_value, value))
    return value


def parse_value(raw_value, min_value, max_value, old_value=0):
    if raw_value[-1] == '%':
        value = percental_value(float(raw_value[:-1]), min_value, max_value)
    else:
        value = int(raw_value)
    value = trim_value(old_value + value - min_value, min_value, max_value)
    return value


def get_percentage(value, min_value, max_value):
    percent = (value - min_value) / (max_value - min_value) * 100
    rounded_percent = int(percent + 0.5)
    return rounded_percent


def calc_color(temperature):
    r = int(2.55*(100-temperature))
    b = int(2.55*temperature)
    g = b
    return "#{:02x}{:02x}{:02x}".format(r, g, b)

def get_icon(brightness):
    icons = ["", "", "", "", "", "", ""]
    return icons[int(brightness/101*len(icons))]


def percent_to_value(percent, min_value, max_value):
    value = int(min_value + percent/100 * (max_value - min_value))
    return min(max_value, max(min_value, value))


def value_to_percent(value, min_value, max_value):
    return int((value-min_value) / (max_value-min_value) * 100)


if args.lamp:
    lamps = [LAMPS[int(args.lamp)],]
else:
    lamps = LAMPS


if args.setb:
    percent = int(args.setb)
    value = percent_to_value(percent, MINBRIGHTNESS, MAXBRIGHTNESS)
    b.set_light(lamps, "bri", value)

if args.incb:
    old_value   = b.get_light(lamps[0], "bri")
    old_percent = value_to_percent(old_value, MINBRIGHTNESS, MAXBRIGHTNESS)
    new_percent = old_percent + int(args.incb)
    new_value   = percent_to_value(new_percent, MINBRIGHTNESS, MAXBRIGHTNESS)
    b.set_light(lamps, "bri", new_value)

if args.decb:
    old_value   = b.get_light(lamps[0], "bri")
    old_percent = value_to_percent(old_value, MINBRIGHTNESS, MAXBRIGHTNESS)
    new_percent = old_percent - int(args.decb)
    new_value   = percent_to_value(new_percent, MINBRIGHTNESS, MAXBRIGHTNESS)
    b.set_light(lamps, "bri", new_value)

if args.getb:
    value = b.get_light(lamps[0], "bri")
    percent = value_to_percent(value, MINBRIGHTNESS, MAXBRIGHTNESS)
    print(percent)

if args.sett:
    percent = 100-int(args.sett)
    value = percent_to_value(percent, MINTEMPERATURE, MAXTEMPERATURE)
    b.set_light(lamps, "ct", value)

if args.inct:
    old_value   = b.get_light(lamps[0], "ct")
    old_percent = 100-value_to_percent(old_value, MINTEMPERATURE, MAXTEMPERATURE)
    new_percent = old_percent + int(args.inct)
    new_value   = percent_to_value(100-new_percent, MINTEMPERATURE, MAXTEMPERATURE)
    b.set_light(lamps, "ct", new_value)

if args.dect:
    old_value   = b.get_light(lamps[0], "ct")
    old_percent = 100-value_to_percent(old_value, MINTEMPERATURE, MAXTEMPERATURE)
    new_percent = old_percent - int(args.dect)
    new_value   = percent_to_value(100-new_percent, MINTEMPERATURE, MAXTEMPERATURE)
    b.set_light(lamps, "ct", new_value)

if args.gett:
    value = b.get_light(lamps[0], "ct")
    percent = 100-value_to_percent(value, MINTEMPERATURE, MAXTEMPERATURE)
    print(percent)

if args.state:
    value       = b.get_light(lamps[0], "ct")
    temperature = 100-get_percentage(value, MINTEMPERATURE, MAXTEMPERATURE)
    color       = calc_color(temperature)
    value      = b.get_light(lamps[0], "bri")
    brightness = get_percentage(value, MINBRIGHTNESS, MAXBRIGHTNESS)
    icon       = get_icon(brightness)
    text    = "<span color=\'{}\'>{}</span>".format(color, icon)
    tooltip = "brightness:  {:3}\\ntemperature: {:3}".format(brightness, temperature)
    print('{"text": "' + text + '", "tooltip": "' + tooltip + '"}')

if args.toggle:
    state = b.get_light(lamps[0], "on")
    b.set_light(lamps, "on", not state)


#value = calc_new_brightness(args.brightness)
#print("set to", value)
#b.set_light(1, "bri", value)
