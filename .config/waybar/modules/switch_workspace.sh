#! /bin/bash


chworkspace(){
        sleep 0.2
        swaymsg "workspace $1"
}


case $1 in
   [1-9] | 10)
      chworkspace $1
      ;;
   up)
      chworkspace prev
      ;;
   down)
      chworkspace next
      ;;
esac

pkill -SIGRTMIN+11 waybar
