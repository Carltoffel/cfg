#!/bin/bash

SCRIPT_DIR=$(dirname $(realpath $0))
STATE_FILE="$SCRIPT_DIR/eye_reminder_state"

TIMER_INTERVAL=1200
BREAK_INTERVAL=20

# Initialize state file if it doesn't exist
if [ ! -f $STATE_FILE ]; then
    echo "timer_state=off" > $STATE_FILE
    echo "notify_state=notify" >> $STATE_FILE
    echo "timer_status=running" >> $STATE_FILE
    echo "start_time=$(date +%s)" >> $STATE_FILE
fi

# Read the state file
TIMER_STATE=$(grep "timer_state=" $STATE_FILE | cut -d '=' -f 2)
NOTIFY_STATE=$(grep "notify_state=" $STATE_FILE | cut -d '=' -f 2)
TIMER_STATUS=$(grep "timer_status=" $STATE_FILE | cut -d '=' -f 2)
START_TIME=$(grep "start_time=" $STATE_FILE | cut -d '=' -f 2)
CURRENT_TIME=$(date +%s)

function update_state_file {
    sed -i "s/timer_state=.*/timer_state=$1/" $STATE_FILE
    sed -i "s/notify_state=.*/notify_state=$2/" $STATE_FILE
    sed -i "s/timer_status=.*/timer_status=$3/" $STATE_FILE
    sed -i "s/start_time=.*/start_time=$4/" $STATE_FILE
}

function notify {
    if [ "$NOTIFY_STATE" == "notify" ]; then
        notify-send "Augenpause" "Schau 20 Sekunden lang weg!"
        spd-say "Augenpause"
    fi
}

# Toggle timer state with right-click
if [ "$1" == "toggle_timer" ]; then
    if [ "$TIMER_STATE" == "off" ]; then
        TIMER_STATE="on"
        TIMER_STATUS="running"
        START_TIME=$CURRENT_TIME
        update_state_file "$TIMER_STATE" "$NOTIFY_STATE" "$TIMER_STATUS" "$START_TIME"
    elif [ "$TIMER_STATE" == "on" ]; then
        TIMER_STATE="off"
        TIMER_STATUS="stopped"
        START_TIME=$CURRENT_TIME
        update_state_file "$TIMER_STATE" "$NOTIFY_STATE" "$TIMER_STATUS" "$START_TIME"
    fi
fi

# Toggle notification state with middle-click
if [ "$1" == "toggle_notify" ]; then
    if [ "$NOTIFY_STATE" == "notify" ]; then
        NOTIFY_STATE="mute"
        update_state_file "$TIMER_STATE" "$NOTIFY_STATE" "$TIMER_STATUS" "$START_TIME"
    elif [ "$NOTIFY_STATE" == "mute" ]; then
        NOTIFY_STATE="notify"
        update_state_file "$TIMER_STATE" "$NOTIFY_STATE" "$TIMER_STATUS" "$START_TIME"
    fi
fi

# Handle left-click to start 20 seconds timer
if [ "$1" == "click" ]; then
    TIMER_STATE="on"
    TIMER_STATUS="break"
    START_TIME=$CURRENT_TIME
    update_state_file "$TIMER_STATE" "$NOTIFY_STATE" "$TIMER_STATUS" "$START_TIME"
fi

if [ "$TIMER_STATE" == "off" ]; then
    EYE=""
elif [ "$TIMER_STATE" == "on" ]; then
    EYE=""
fi

if [ "$TIMER_STATE" == "off" ]; then
    BELL=""
elif [ "$NOTIFY_STATE" == "notify" ]; then
    BELL=" "
elif [ "$NOTIFY_STATE" == "mute" ]; then
    BELL=" "
fi

if [ "$TIMER_STATUS" == "running" ]; then
    DURATION=$TIMER_INTERVAL
elif [ "$TIMER_STATUS" == "break" ]; then
    DURATION=$BREAK_INTERVAL
else
    DURATION=0
fi

REMAINING_TIME=$((DURATION - (CURRENT_TIME - START_TIME)))

if [ $REMAINING_TIME -le 0 ]; then
    if [ "$TIMER_STATUS" == "running" ]; then
        TIMER_STATUS="expired"
        update_state_file "$TIMER_STATE" "$NOTIFY_STATE" "$TIMER_STATUS" "$CURRENT_TIME"
        notify
    elif [ "$TIMER_STATUS" == "break" ]; then
        if [ "$NOTIFY_STATE" == "notify" ]; then
            spd-say "OK\!"
        fi
        TIMER_STATUS="running" 
        update_state_file "$TIMER_STATE" "$NOTIFY_STATE" "$TIMER_STATUS" "$CURRENT_TIME"
    fi
fi

ICON=" $EYE$BELL "
if [ "$TIMER_STATUS" == "stopped" ]; then
    TOOLTIP="Inaktiv"
elif [ "$TIMER_STATUS" == "expired" ]; then
    REMAINING_TIME=$((-REMAINING_TIME))
    TIME=$(printf "%02d:%02d" $((REMAINING_TIME / 60)) $((REMAINING_TIME % 60)))
    TOOLTIP="Vor $TIME"
else
    TIME=$(printf "%02d:%02d" $((REMAINING_TIME / 60)) $((REMAINING_TIME % 60)))
    TOOLTIP="Noch $TIME"
fi

echo "{\"text\":\"$ICON\",\"tooltip\":\"$TOOLTIP\",\"class\":\"$TIMER_STATUS\"}"
