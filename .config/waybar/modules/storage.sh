#!/bin/sh

mount="/"
warning=20
critical=10

df -h -P -l "$mount" | awk -v warning=$warning -v critical=$critical '
/\/.*/ {
  text=$4
  tooltip="Filesystem: "$1"\\nSize: "$2"\\nUsed: "$3"\\nAvail: "$4"\\nUse%: "$5"\\nMounted on: "$6
  use=$5
}
END {
  class="normal"
  gsub(/%$/,"",use)
  if ((100 - use) < critical) {
    class="critical"
  } else if ((100 - use) < warning) {
    class="warning"
  }
  print "{\"text\":\""text"\", \"percentage\":"use", \"tooltip\":\""tooltip"\", \"class\":\""class"\"}"
}
' | jq --unbuffered --compact-output

