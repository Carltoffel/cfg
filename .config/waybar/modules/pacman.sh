#!/bin/bash

updates="$(checkupdates --nocolor | column --table)"
num_of_updates=$(echo -n "$updates" | grep -c '^')

if [ -n "$updates" ]; then
   class='pending'
   text=""
else
   class=''
   text=""
fi

if [[ $num_of_updates == 1 ]]; then
    tooltip="1 update:\n"
else
    tooltip="$num_of_updates updates:\n"
fi
tooltip="$tooltip$(echo -n "$updates" | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g')"
    # replace newlines with literal \n

echo '{"text": "'"$text"'", "class": "'"$class"'", "tooltip": "'"$tooltip"'"}'
