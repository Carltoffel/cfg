#! /usr/bin/env python

import json
import subprocess
import datetime
import re


def get_watson_status():
    try:
        project_result = subprocess.run(["watson", "status", "-p"], capture_output=True, text=True)
        status_result = subprocess.run(["watson", "status"], capture_output=True, text=True)
        log_result = subprocess.run(["watson", "log", "--day", "--json", "--current"], capture_output=True, text=True)

        project = project_result.stdout.strip()
        status_output = status_result.stdout.strip()
        log_output = log_result.stdout.strip()

        # Laufzeit des aktuellen Projekts berechnen
        match = re.search(r'\((\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})', status_output)
        if match:
            start_time = datetime.datetime.strptime(match.group(1), "%Y-%m-%d %H:%M:%S")
            now = datetime.datetime.now()
            elapsed = now - start_time
            elapsed_str = str(elapsed).split('.')[0][:-3]  # Entfernt Mikrosekunden und Sekunden
        else:
            elapsed_str = "Unbekannte Zeit"

        # Log für den heutigen Tag auswerten
        log_entries = json.loads(log_output)
        max_length = max(len(entry['project']) for entry in log_entries) if log_entries else 0
        log_texts = []
        for entry in reversed(log_entries):  # Neueste zuerst
            start = datetime.datetime.fromisoformat(entry["start"]).strftime("%H:%M")
            stop = "now" if entry["id"] == "current" else datetime.datetime.fromisoformat(entry["stop"]).strftime("%H:%M")
            log_texts.append(f"{entry['project'].ljust(max_length)} {start} - {stop}")
        log_text = "\n".join(log_texts)

        if project == "No project started.":
            data = {"text": "⏱️", "tooltip": log_text, "class": "stopped"}
        else:
            data = {
                "text": f"⏱️ {project} ({elapsed_str})",
                "tooltip": log_text,
                "class": "running"
            }

        print(json.dumps(data))
    except Exception as e:
        print(json.dumps({"text": "!", "tooltip": f"Fehler: {str(e)}", "class": "error"}))


if __name__ == "__main__":
    get_watson_status()
