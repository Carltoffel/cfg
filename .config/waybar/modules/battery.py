#! /usr/bin/env python3
import subprocess
import json

def get_battery_status():
    try:
        output = subprocess.check_output(
            "upower -i /org/freedesktop/UPower/devices/battery_BAT0", shell=True, text=True)
        return output
    except subprocess.CalledProcessError:
        return ""

def parse_battery_info(battery_info):
    lines = battery_info.split('\n')
    info = {}

    for line in lines:
        if 'percentage:' in line:
            info['percentage'] = int(line.split(':')[1].strip().strip('%'))
        elif 'time to empty' in line:
            info['time_empty'] = line.split(':')[1].strip()
        elif 'time to full' in line:
            info['time_full'] = line.split(':')[1].strip()
        elif 'state:' in line:
            info['state'] = line.split(':')[1].strip()

    return info

def format_time(time_str):
    if not time_str:
        return "unbekannt"
    try:
        hours, remainder = divmod(float(time_str.split()[0]), 1)
        minutes = int(remainder * 60)
        return f"{int(hours):02d}:{minutes:02d}"
    except:
        return "Fehlerhafte Zeit"

def get_icon(percentage, state):
    charging_icons = "󰢟󰢜󰂆󰂇󰂈󰢝󰂉󰢞󰂊󰂋󰂅"
    discharging_icons = "󰂎󰁺󰁻󰁼󰁽󰁾󰁿󰂀󰂁󰂂󰁹"

    if state == 'charging':
        icons = charging_icons
    else:
        icons = discharging_icons

    index = min(percentage // (100 // len(icons)), len(icons) - 1)
    return icons[index]

def main():
    battery_info = get_battery_status()
    if battery_info:
        parsed_info = parse_battery_info(battery_info)
        icon = get_icon(parsed_info.get('percentage', 0), parsed_info.get('state', ''))

        battery_class = parsed_info.get('state')

        time_info = ""
        if battery_class == 'charging':
            time_info = f"{format_time(parsed_info.get('time_full', ''))} bis voll"
        elif battery_class == 'discharging':
            time_info = f"{format_time(parsed_info.get('time_empty', ''))} bis leer"
        elif battery_class == 'fully-charged':
            time_info = "Voll"

        tooltip = f"{parsed_info.get('percentage', 0)}%\n{time_info}"

        if parsed_info.get('percentage', 100) <= 15:
            battery_class += " critical"
        elif parsed_info.get('percentage', 100) <= 30:
            battery_class += " warning"

        output = {"text": icon, "tooltip": tooltip, "class": battery_class}
        print(json.dumps(output, ensure_ascii=False))
    else:
        print("Fehler: Batterieinformationen konnten nicht abgerufen werden.")

if __name__ == "__main__":
    main()

