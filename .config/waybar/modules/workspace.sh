#! /bin/bash

_ifs=$IFS
IFS=

tmpfile=~/.config/waybar/modules/workspace.tmp

if [ -z $1 ]; then
   workspace_info=`swaymsg -pt get_workspaces`
   if [[ "`cat $tmpfile`" != "$workspace_info" ]]; then
      echo $workspace_info > $tmpfile
      pkill -SIGRTMIN+10 waybar
   fi
else
   workspace=`grep -A 3 -P "Workspace $1( |$)" $tmpfile`

   if [ -z $workspace ]; then
      class=invisible
      text=
   else
      if [[ `grep Workspace <<< $workspace` == *focused* ]]; then
         class=focused
      fi

      # get application list -> "H[termite firefox]"
      applications=$(grep Representation <<< $workspace | cut -f2 -d':')
      # remove leading letter and brackets -> "termite firefox"
      applications=$(sed 's/\([VHST]\[\|\]\|(null)\)//g' <<< $applications)
      # fix for jitsi with space in name
      applications=$(sed 's/Jitsi Meet/Jitsi/g' <<< $applications)
      # fix for minecraft with space in name
      applications=$(sed 's/Minecraft\*\? \([^ ]\+\|Launcher\)/Minecraft/g' <<< $applications)

      IFS=$_ifs

      applications=($applications)
      icons=""
      for application in ${applications[@]}; do
              icons="${icons} $(~/.config/waybar/modules/icon_mapper.sh $application)"
      done

      text="$1$icons"
   fi

   echo -e "{\"text\": \"$text\", \"class\": \"$class\"}"
fi
