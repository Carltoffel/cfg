#!/bin/bash

tmpfile=~/.config/waybar/modules/spotify.tmp
curr_player=spotify

repl() {
    str="$(printf "$1"'%.s' $(seq 0 "$2"))"
    echo -e "${str:1}"
}

sec_to_timestamp() {
    min=$(echo "$1 / 60" | bc)
    sec=$(echo "$1/1 - 60 * $min" | bc)
        # pos/1 cuts of decimal places
    if [[ $sec -lt 10 ]]; then sec=0$sec; fi
    echo "$min:$sec"
}

bar_tip() {
    bars=(" " "▁" "▂" "▃" "▄" "▅" "▆" "▇" "█")
    index=$(echo "$1 * 8 / 100" | bc)
    echo ${bars[$index]}
}

status=$(playerctl status --player=${curr_player})

artist=$(playerctl metadata --player=${curr_player} --format '{{artist}}')
title=$(playerctl metadata --player=${curr_player} --format '{{title}}')
album=$(playerctl metadata --player=${curr_player} --format '{{album}}')

max_len=$(echo -e "${#title} \n ${#artist} \n ${#album}" | sort -n | tail -1)
max_len=$(echo "$max_len - 7" | bc)
max_len=$(echo -e "$max_len \n 10" | sort -n | tail -1)

pos=$(playerctl position --player=${curr_player})
len=$(playerctl metadata mpris:length --player=${curr_player})
len=$(echo "$len / 1000000" | bc)
rel_pos=$(echo "$max_len * $pos / ($len)" | bc)
remaining=$(echo "$max_len - $rel_pos -1" | bc)

current_time=$(sec_to_timestamp $pos)
track_time=$(sec_to_timestamp $len)

bar="$current_time $(repl ━ $rel_pos)●$(repl ─ $remaining) $track_time"

vol=$(echo "$(playerctl -p $curr_player volume) * 100 /1" | bc)
if [[ $vol -gt 67 ]]
then
    rest=$(echo "100 * ($vol - 67) / 33" | bc)
    vol_bar_top=$(bar_tip $rest)
    vol_bar_mid="█"
    vol_bar_bot="█"

    bar="$bar 󰕾"
elif [[ $vol -gt 33 ]]
then
    rest=$(echo "100 * ($vol - 33) / 34" | bc)
    vol_bar_mid=$(bar_tip $rest)
    vol_bar_bot="█"

    bar="$bar 󰖀"
else
    rest=$(echo "100 * $vol / 33" | bc)
    vol_bar_bot=$(bar_tip $rest)

    bar="$bar 󰕿"
fi

extra_lenght=9
spaces=$(echo "$max_len - ${#title} + $extra_lenght" | bc)
tt_title="$title$(repl " " $spaces)$vol_bar_top"
spaces=$(echo "$max_len - ${#artist} + $extra_lenght" | bc)
tt_artist="$artist$(repl " " $spaces)$vol_bar_mid"
spaces=$(echo "$max_len - ${#album} + $extra_lenght" | bc)
tt_album="$album$(repl " " $spaces)$vol_bar_bot"

if [[ $status == "Playing" ]]; then
    status_icon="󰏤"
elif [[ $status == "Paused" ]]; then
    status_icon="󰐊"
elif [[ $status == "Stopped" ]]; then
    status_icon="󰓛"
fi

class=$(swaymsg -pt get_workspaces | grep Workspace\ O | sed 's/.*(\([^]]*\)).*/\1/g')

echo -e '{"text": "'"$status_icon $title"'",' \
         '"class": "'"$class"'",' \
         '"short": "'"$short"'",' \
         '"tooltip": "󰎆 '"$tt_title"'\\n󰀉 '"$tt_artist"'\\n󰀥 '"$tt_album"'\\n'"$bar"'"}'

