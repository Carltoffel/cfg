#!/bin/bash

# Dateiname für temporäre Speicherung der Prozesszeiten
temp_file=~/.config/waybar/modules/cpu.tmp
out_file=~/.config/waybar/modules/cpu.out

last_update=$(date +%s -r "$temp_file")
current_time=$(date +%s)
time_diff=$((current_time - last_update))

if [[ $time_diff < 10 ]]; then
    cat $out_file
    exit
fi

# Grenzwert für die CPU-Auslastung, ab dem die Klasse 'load' gesetzt wird
cpu_load_threshold=80

# Prüfen, ob die temporäre Datei existiert. Wenn nicht, leere Datei erstellen.
if [ ! -f "$temp_file" ]; then
    touch "$temp_file"
fi

# Zeitdifferenz in Sekunden seit der letzten Änderung der Datei ermitteln

# Aktuelle Prozesszeiten ermitteln, wobei kworker Prozesse ignoriert werden
current_data=$(ps -e | awk 'NR>1 && $3!="00:00:00" && !($4 ~ /^kworker/) {
    time_str = $3;
    cmd = substr($0, index($0, $4));
    split(time_str, time, ":");
    sec = time[1]*3600 + time[2]*60 + time[3];
    proc[cmd]+=sec
} END {for (p in proc) print p, proc[p]}' | sort)

# Vorherige Prozesszeiten lesen
previous_data=$(cat "$temp_file")

# Aktuelle Daten für den nächsten Lauf speichern
echo "$current_data" > "$temp_file"

# Die prozentuale Differenz berechnen und für den Tooltip formatieren
tooltip_data=$(awk -v time_diff="$time_diff" 'NR==FNR {proc[$1]=$2; next} {
    diff = $2 - proc[$1];
    if (diff > 0) {
        percent = int((diff / time_diff) * 100);
        print $1 ":", percent "%";
    }
}' <(echo "$previous_data") <(echo "$current_data") | sort -t: -k2 -nr)

# Tooltip formatieren und Gesamtauslastung der CPU berechnen
total_cpu_usage=0
tooltip=""
while IFS= read -r line; do
    cpu_usage=$(echo "$line" | awk -F":" '{print $2}' | tr -d '%')
    total_cpu_usage=$((total_cpu_usage + cpu_usage))
    tooltip+="$line\n"
done <<< "$tooltip_data"

# Klassenwert basierend auf der Gesamtauslastung der CPU setzen
class=""
if [ "$total_cpu_usage" -ge "$cpu_load_threshold" ]; then
  class="load"
fi

# JSON-Ausgabe generieren
output="{\"text\":\"CPU $total_cpu_usage%\",\"tooltip\":\"${tooltip%\\n}\",\"class\":\"$class\"}"
echo $output
echo $output > $out_file

