#! /bin/env bash

function bind_workspaces_to() {
    case $# in
        1)
            for i in {1..10}; do
                swaymsg "workspace $i output $1"
                swaymsg "workspace $i; move workspace to output $1"
            done
            ;;
        2)
            for i in {1..5}; do
                swaymsg "workspace $i output $1"
                swaymsg "workspace $i; move workspace to output $1"
            done
            for i in {6..10}; do
                swaymsg "workspace $i output $2"
                swaymsg "workspace $i; move workspace to output $2"
            done
            ;;
        3)
            for i in {1..3}; do
                swaymsg "workspace $i output $1"
                swaymsg "workspace $i; move workspace to output $1"
            done
            for i in {4..7}; do
                swaymsg "workspace $i output $2"
                swaymsg "workspace $i; move workspace to output $2"
            done
            for i in {8..10}; do
                swaymsg "workspace $i output $3"
                swaymsg "workspace $i; move workspace to output $3"
            done
            ;;
    esac
}

if [ $# -eq 1 ]; then
    echo sleep $1
    sleep $1
fi

outputs=$(swaymsg -pt get_outputs | grep -e "Position" -B 3 | tr -d '\n' | sed -e 's/--/\n/g;s/$/\n/' | sed 's/Output \([^ ]*\).*Position: \([0-9,]*\)/\2 \1/' | sort -n | cut -f2 -d' ')
    #                               ^ grep outputs            ^ put one output per line                 ^ reorder: position first, output name second            ^ sort outputs from left to right

echo "outputs discovered: $(echo $outputs|tr '\n' ' ')"
bind_workspaces_to $outputs
