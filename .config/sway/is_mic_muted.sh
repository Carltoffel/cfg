#! /bin/bash

#!/bin/bash

# Ermitteln der Standard-Audioquelle
default_source=$(pactl info | grep 'Default Source' | cut -d' ' -f3)

# Überprüfen, ob die Standard-Audioquelle stummgeschaltet ist
muted=$(pactl list sources | grep -A 10 "Name: $default_source" | grep 'Mute' | awk '{print $2}')

if [ "$muted" = "yes" ]; then
    exit 0
else
    exit 1
fi

