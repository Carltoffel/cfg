#! /bin/bash

darkmode_file="$HOME/.config/sway/darkmode"

initialize_darkmode_file() {
    if [ ! -f "$darkmode_file" ]; then
        echo "0" > "$darkmode_file"
    fi
}

read_darkmode_value() {
    cat "$darkmode_file"
}

toggle_darkmode() {
    local darkmode=$(read_darkmode_value)

    if [ "$darkmode" == "1" ]; then
        echo "0" > "$darkmode_file"
        apply_lightmode
    else
        echo "1" > "$darkmode_file"
        apply_darkmode
    fi
}

apply_alacritty_lightmode() {
    sed -i '/\/themes\/.*Light.toml/s/^#*//' ~/.config/alacritty/alacritty.toml
    sed -i '/\/themes\/.*Dark.toml/s/^#*/#/' ~/.config/alacritty/alacritty.toml
}

apply_alacritty_darkmode() {
    sed -i '/\/themes\/.*Light.toml/s/^#*/#/' ~/.config/alacritty/alacritty.toml
    sed -i '/\/themes\/.*Dark.toml/s/^#//' ~/.config/alacritty/alacritty.toml
}

apply_nvim_lightmode() {
    sed -i '/vim.o.background/s/"dark"/"light"/' ~/.config/nvim/lua/custom/background.lua
    kill -SIGUSR1 $(pgrep nvim)
}

apply_nvim_darkmode() {
    sed -i '/vim.o.background/s/"light"/"dark"/' ~/.config/nvim/lua/custom/background.lua
    kill -SIGUSR1 $(pgrep nvim)
}

apply_tmux_lightmode() {
    sed -i '/solarized_flavour/s/"dark"/"light"/' ~/.config/tmux/tmux.conf
    tmux source ~/.config/tmux/tmux.conf
}

apply_tmux_darkmode() {
    sed -i '/solarized_flavour/s/"light"/"dark"/' ~/.config/tmux/tmux.conf
    tmux source ~/.config/tmux/tmux.conf
}

apply_zsh_lightmode() {
    sed -i '/# light-theme$/s/^#//' ~/.config/zsh/.zshrc
}
apply_zsh_darkmode() {
    sed -i '/# light-theme$/s/^/#/' ~/.config/zsh/.zshrc
}

apply_lightmode() {
    apply_alacritty_lightmode
    apply_nvim_lightmode
    apply_tmux_lightmode
    apply_zsh_lightmode
}

apply_darkmode() {
    apply_alacritty_darkmode
    apply_nvim_darkmode
    apply_tmux_darkmode
    apply_zsh_darkmode
}


initialize_darkmode_file
toggle_darkmode
