#! /bin/bash

if ~/.config/sway/is_mic_muted.sh; then
   volumectl -m % && paplay ~/.config/sway/unmute.ogg &
else
   volumectl -m % && paplay ~/.config/sway/mute.ogg &
fi
pkill -SIGRTMIN+9 waybar
