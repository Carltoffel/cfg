#! /bin/env python3
import subprocess


def no_space(string):
    return "%{" + string + "%}"

clear_color = no_space("\x1b[0m")
RED = "\033[0;31m"
GREEN = "\033[0;32m"
BLUE = "\033[0;34m"
YELLOW = "\033[0;33m"

def rgb(r, g, b, string):
    color_string = no_space("\x1b[38;2;{};{};{}m".format(r, g, b))
    return color_string + string + clear_color

def color(c, string):
    return no_space(c) + string + clear_color

def red(string):
    return color(RED, string)

def green(string):
    return color(GREEN, string)

def blue(string):
    return color(BLUE, string)

def yellow(string):
    return color(YELLOW, string)

def get_git_file_infos(gitfile):
    stage = gitfile[1] == " "

    if stage:
        status = gitfile[0]
    else:
        status = gitfile[1]

    name = gitfile[3:]

    return stage, status, name



git_short_status = subprocess.getoutput("git status -s")
git_branch = subprocess.getoutput("git rev-parse --abbrev-ref HEAD")

if git_short_status[:5] == "fatal":
    exit(1)

stage_files = []
flags = ""

if git_short_status:
    for gitfile in git_short_status.split("\n"):
        stage, status, name = get_git_file_infos(gitfile)

        if stage:
            stage_files.append(name)
        else:
            flags += status

rprompt = "< "
if git_short_status == "":
    rprompt += green(git_branch)
else:
    rprompt += red(git_branch)

if stage_files:
    rprompt += " ("
    for f in stage_files[:-1]:
        rprompt += yellow(f) + ", "
    rprompt += yellow(stage_files[-1]) + ")"

if 0 < len(flags) < 10:
    rprompt += " " + red(flags)

print(rprompt)
