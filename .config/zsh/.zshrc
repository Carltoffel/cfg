# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh//.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Load completions
autoload -U compinit && compinit

# Load package manager
source /usr/share/zinit/zinit.zsh

# Plugins
zinit ice depth=1; zinit light romkatv/powerlevel10k
    # prompt
zinit light Aloxaf/fzf-tab
zinit light zsh-users/zsh-syntax-highlighting
    # syntax highlighting
zinit light zsh-users/zsh-completions
    # Autocompletion
zinit light zsh-users/zsh-autosuggestions
    # Command suggestions based on history

# Add in snippets
zinit snippet OMZP::command-not-found
    # Show packages to install the command that wasn't found
zinit snippet OMZP::sudo
    # ESC+ESC inserts sudo
zinit snippet OMZP::vi-mode


zinit cdreplay -q

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k-*.zsh.
if [ -z $DISPLAY ]; then
    [[ ! -f ~/.config/zsh/.p10k-tty.zsh ]] || source ~/.config/zsh/.p10k-tty.zsh
else
    [[ ! -f ~/.config/zsh/.p10k-sway.zsh ]] || source ~/.config/zsh/.p10k-sway.zsh
fi

# Keybindings
bindkey -v
    # vi mode
bindkey '^N' history-search-backward
bindkey '^L' history-search-forward
    # For autosuggestions

# History
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:*' default-color $'\033[30m' # light-theme
zstyle ':fzf-tab:*' fzf-flags --color=light # light-theme
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'
export FZF_DEFAULT_OPTS="--color=light" # light-theme
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=14,underline"

source ${ZDOTDIR}/alias.ini
function cfg() {
        git --git-dir=$HOME/.cfg.git/ --work-tree=$HOME/ $@
}
compdef cfg=git

# Shell integrations
eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"
setopt autocd

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh
