-- Detect correct filetype everywhere
vim.g.tex_flavor = "latex"

vim.o.spell     = true
vim.o.spelllang = "de_de"

-- Folding
vim.g.tex_fold_enabled = 1
vim.o.foldmethod = "syntax"
