HOME = os.getenv("HOME")

require('custom.general')
require('custom.plugins')
require('custom.lsp')
require('custom.visual')
require('custom.maps')
require('custom.ftplugin')
