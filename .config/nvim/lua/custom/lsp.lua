local lsp = require'lspconfig'

-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local signature_setup = {
    capabilities = capabilities,
    on_attach = function(_, _)
        require'lsp_signature'.on_attach({
            bind = true,
            handler_opts = {
                border = "single"
            },
            use_lspsage = true,
        })
    end,
}

vim.o.updatetime = 250
vim.cmd [[autocmd CursorHold * lua vim.diagnostic.open_float(nil, {focus=false})]]
vim.diagnostic.config({
    virtual_text = false,
    float = {
        source = "always",  -- Or "if_many"
    },
})


lsp.lua_ls.setup{
    cmd = { "lua-language-server" },
    on_attach = signature_setup.on_attach,
    capabilities = signature_setup.capabilities
}


lsp.fortls.setup {
    cmd = { "fortls", "--hover_signature", "--enable_code_actions", "--lowercase_intrinsics" },
    root_dir = lsp.util.root_pattern('.git'),
    on_attach = signature_setup.on_attach,
    capabilities = signature_setup.capabilities,
}

lsp.texlab.setup {
    cmd = { "texlab" },
    on_attach = signature_setup.on_attach,
    capabilities = signature_setup.capabilities
}

lsp.pylsp.setup {
    cmd = { "pylsp" },
    on_attach = signature_setup.on_attach,
    capabilities = signature_setup.capabilities
}

--[[
lsp.ltex.setup{
    cmd = { "ltex-ls" },
    on_attach = signature_setup.on_attach,
    capabilities = signature_setup.capabilities
}
]]--

lsp.bashls.setup{
    cmd = { "bash-language-server", "start" },
    on_attach = signature_setup.on_attach,
    capabilities = signature_setup.capabilities
}
