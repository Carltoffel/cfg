vim.o.termguicolors = true
require('custom.background')
require('colorizer').setup()
vim.o.colorcolumn   = '70'
vim.cmd[[colorscheme solarized-flat]]
vim.cmd[[highlight ExtraWhitespace guibg=white]]
vim.cmd[[match ExtraWhitespace /\s\+$\|\t/]]

vim.cmd[[
if &diff
   highlight DiffAdd    cterm=bold ctermfg=8 ctermbg=47 gui=bold guifg=bg guibg=lightgreen
   highlight DiffDelete cterm=bold ctermfg=8 ctermbg=167 gui=bold guifg=bg guibg=lightred
   highlight DiffChange cterm=bold ctermfg=8 ctermbg=192 gui=bold guifg=bg guibg=lightyellow
   highlight DiffText   cterm=bold ctermfg=192 ctermbg=52 gui=bold guifg=fg guibg=darkred
endif
]]


function _G.ToggleBackground()
    local bg
    if vim.o.background == "dark" then
        bg = "light"
    else
        bg = "dark"
    end
    local fw = io.open(HOME.."/.config/nvim/lua/custom/background.lua", "w")
    fw:write('vim.o.background = "'..bg..'"')
    fw:close()
    vim.o.background = bg
end


function _G.CheckForBackgroundChange()
    local file = vim.fn.stdpath('config') .. "/lua/custom/background.lua"
    local f = io.open(file, "r")
    if f then
        local line = f:read("*line")
        if line then
            -- Extrahiere den Wert nach dem Gleichheitszeichen
            local bg = line:match('vim.o.background = "(%w+)"')
            if bg then
                vim.o.background = bg
            end
        end
        f:close()
    end
end


vim.api.nvim_create_autocmd("Signal", {
    pattern = "SIGUSR1",
    callback = _G.CheckForBackgroundChange,
    group = vim.api.nvim_create_augroup("BackgroundWatcher1", {clear = true})
})
