--[[=================--

  MAPPINGS

--=================]]--

vim.g.mapleader      = " "
vim.o.mouse          = "a"

local wk = require("which-key")

-- We use vim.cmd to define the mapping for <Leader>s because it allows us to open 
-- the command-line mode and wait for the user to input the search and replace term.
-- This would not be possible using which-key's register function because it expects 
-- the mapped command to end with <CR>, which would immediately close the command-line mode.
vim.cmd("nmap <Leader>s :%s//g<Left><Left>")

-- normal mode
wk.add({
    { "<Leader><Leader>", "za", desc = "Toggle Folding" },
    { "<Leader>b", "<cmd>:lua ToggleBackground()<cr>", desc = "Toggle Dark/Light" },
    { "<Leader>f", require('telescope.builtin').find_files, desc = "Find files" },
    { "<Leader>r", "<cmd>:lua vim.lsp.buf.rename()<cr>", desc = "Rename" },
    { "<Leader>s", desc = "Search&Replace" },
    { "<Leader>t", "<cmd>:NvimTreeFocus<cr>", desc = "File Tree" },
    {
        mode = {"i"},
        { "<C-N>", "<Plug>luasnip-next-choice", desc = "Next choice", mode = "i" },
    },
    {
        mode = {"x"},
        { "<Leader>a", group = "Align Code" },
        { "<Leader>aa", function() require'align'.align_to_char({1, true}) end,         desc = "align to 1 character" },
        { "<Leader>as", function() require'align'.align_to_char({2, true, true}) end,   desc = "align to 2 characters" },
        { "<Leader>aw", function() require'align'.align_to_string({false, true, true}) end, desc = "align to string" },
        { "<Leader>ar", function() require'align'.align_to_string({true, true, true}) end,  desc = "align to pattern" },
    }
})
