--[[=================--

  GENERAL SETTINGS
  - numbering
  - undo history
  - backup files
  - indentation
  - window splitting
  - searching

--=================]]--


-- numbering
vim.o.number         = true
vim.o.relativenumber = true
-- switch to normal numbers in command line mode (:)
-- TODO translate to lua
vim.cmd[[autocmd CmdlineEnter * if &filetype !=# 'NvimTree' && &filetype !=# 'vista' | set norelativenumber | redraw! | endif]]
vim.cmd[[autocmd CmdlineLeave * if &filetype !=# 'NvimTree' && &filetype !=# 'vista' | set relativenumber | redraw! | endif]]


-- directories for temporary files
-- undo history
vim.o.undofile  = true
vim.o.undodir   = HOME..'/.cache/nvim/undodir'
-- backups
vim.o.backup    = true
vim.o.backupdir = HOME..'/.config/nvim/backupdir'


-- indentation
TABSIZE          = 4
vim.o.expandtab  = true     -- insert spaces instead of tab
vim.o.tabstop    = TABSIZE  -- size of a tab
vim.o.shiftwidth = TABSIZE  -- width shifted with << and >>


-- splitting windows
vim.o.splitbelow = true
vim.o.splitright = true


-- search
vim.o.ignorecase = true  -- case insensitive
vim.o.smartcase  = true  -- case sensitive when typing capital letters


-- jump to last editing position
-- TODO translate to lua
vim.cmd[[au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif]]

-- folding
vim.o.foldmethod = "expr"
vim.o.foldexpr   = "nvim_treesitter#foldexpr()"


-- providers
-- deactive
vim.g.loaded_python_provider = 0
vim.g.loaded_ruby_provider   = 0
vim.g.loaded_perl_provider   = 0
-- set path for python3
vim.g.python3_host_prog = '/bin/python3'
