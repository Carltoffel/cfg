-- HELP: see :h luasnip
local ls = require'luasnip'

-- some shorthands...
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local types = require("luasnip.util.types")

-- Every unspecified option will be set to the default.
ls.config.set_config({
    history = true,
    -- Update more often, :h events for more info.
    updateevents = "TextChanged,TextChangedI",
    ext_opts = {
        [types.choiceNode] = {
            active = {
                virt_text = { { "choiceNode", "Comment" } },
            },
        },
    },
    -- treesitter-hl has 100, use something higher (default is 200).
    ext_base_prio = 300,
    -- minimal increase in priority.
    ext_prio_increase = 1,
    enable_autosnippets = true,
})

local function copy(args)
    return args[1]
end

local function split_args(args)
    local nodes = {}
    local num = 1
    for arg in string.gmatch(args[1][1], "[^ ,]+") do
        table.insert(nodes, i(num, "real"))
        table.insert(nodes, t(", intent("))
        table.insert(nodes, c(num+1, {t("in"), t("out"), t("inout")}))
        table.insert(nodes, t(")"))
        table.insert(nodes, t({" :: "..arg, ""}))
        num = num+2
    end
    return sn(nil, nodes)
end

ls.add_snippets(nil, {
    python = {
        s("main", {
            t({"def main():", "\t"}), i(0),
            t({"", "", "", "if __name__ == '__main__':", "\tmain()"}),
        }),
    },


    fortran = {
        s("subroutine", {
            t("subroutine "), i(1, "foo"),
            t({"", "\t"}), i(0, "! code ~"),
            t({"", "end subroutine "}), f(function(args) return args[1][1] end, {1})
        }),

        s("function", {
            i(1, "elemental "),
            t("function "),
            i(2, "foo"),
            t("("),
            i(3, "args"),
            t(") result("),
            i(4, "res"),
            t({")", ""}),

            d(5, split_args, {3}),

            i(6, "real"),
            t(" :: "),
            f(copy, 4),
            t({"",""}),

            i(0, "! TODO"),

            t({"", "end function "}),
            f(copy, 2),
        }),

        s("write", {
            t("write(*, "),
            i(1, "*"),
            t(") "),
            i(2, "! \\n")
        }),

        s("program", {
            t("program "),
            i(1, "main"),

            t({"","implicit none", ""}),

            i(2, "! TODO"),

            t({"", "end program "}),
            f(copy, 1),
        }),

        s("use", {
            t("use "),
            i(1, "module"),
            t({", only: &", "\t\t"}),

            i(2, "name"),
        }),

        s("type", {
            t("type "),
            i(1, "name_t"),

            t({"", "\t"}),
            i(0, "! TODO"),

            t({"","end type "}),
            f(copy, 1),
        }),

        s("read file", {
            t("integer :: "),
            i(1, "fileunit"),

            t({"", "", "open(newunit="}),
            f(copy, 1),
            t(", file="),
            i(2, "filename"),
            t({")", ""}),

            t("read("),
            f(copy, 1),
            t(", "),
            i(3, "*"),
            t(") "),
            i(0, "var"),

            t({"", "close("}),
            f(copy, 1),
            t(")"),
        }),

        s("associate", {
            t("associate("),
            i(1, "foo"),
            t(" => "),

            i(2, "bar"),
            t({")", "\t"}),

            i(0, "! TODO"),

            t({"", "end associate"}),
        }),
    },
})

