--[[==================--

  PLUGINS
  - autoinstall packer
  - list of plugins

--==================]]--


-- auto install packer
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/opt/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
    packer_bootstrap = fn.system({'git', 'clone', '--depth', '1',
        'https://github.com/wbthomason/packer.nvim', install_path})
end

-- required because packer is in opt (optional)
vim.cmd[[packadd packer.nvim]]

return require('packer').startup(function(use)
    -- list of plugins
    -- housekeeping/performance
    use {'wbthomason/packer.nvim', opt=true}
    use {'dstein64/vim-startuptime'}

    -- color scheme
    use {'ishan9299/nvim-solarized-lua'}


    -- language server and friends
    use {'neovim/nvim-lspconfig',
        config = function()
            require('custom.lsp')
        end,
        requires = {
            {"hrsh7th/cmp-nvim-lsp"},
            {
                'onsails/lspkind-nvim',
                config = function()
                    require'lspkind'.init()
                end
            },
        }
    }
    -- completion management
    use {'hrsh7th/nvim-cmp',
        Event = "InsertEnter",
        requires = {
            {
                "L3MON4D3/LuaSnip",
                config = function()
                    require('custom.snippets')
                end,
                requires = { 'rafamadriz/friendly-snippets' },
            },
            { "hrsh7th/cmp-buffer", after = "nvim-cmp"},
            { "hrsh7th/cmp-path", after = "nvim-cmp" },
            { "hrsh7th/cmp-nvim-lua", after = "nvim-cmp" },
            { "lukas-reineke/cmp-under-comparator" },
            { 'andersevenrud/cmp-tmux', after = "nvim-cmp" },
            { 'saadparwaiz1/cmp_luasnip', after = "nvim-cmp" },
        },
        config = function()
            require'custom.cmp_init'
        end
    }
    -- show function arguments - floating!
    use {"ray-x/lsp_signature.nvim"}
    --use {'Shougo/echodoc.vim',
    --    config = function()
    --        local cmd = vim.cmd
    --        cmd[[let g:echodoc#enable_at_startup = 1]]
    --        cmd[[let g:echodoc#type = 'floating']]
    --    end
    --}
    --use {'ncm2/float-preview.nvim',
    --    config = function()
    --        vim.cmd[[let g:float_preview#docked = 1]]
    --    end,
    --    event = "InsertEnter"
    --}
    --


    use {
        "folke/which-key.nvim",
        config = function()
            require("which-key").setup {}
        end
    }

    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true }
    }
    require('lualine').setup()

    use 'lervag/vimtex'

    use 'norcalli/nvim-colorizer.lua'

    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.x',
        requires = {
            'nvim-lua/plenary.nvim',
            'nvim-treesitter/nvim-treesitter'
        }
    }

    use {
        "brymer-meneses/grammar-guard.nvim",
        requires = {
            "neovim/nvim-lspconfig",
            "williamboman/nvim-lsp-installer"
        }
    }

    use {
        "nvim-tree/nvim-tree.lua",
        requires = {
            "nvim-tree/nvim-web-devicons"
        }
    }
    -- disable netrw at the very start of your init.lua
    vim.g.loaded_netrw = 1
    vim.g.loaded_netrwPlugin = 1

    -- set termguicolors to enable highlight groups
    vim.opt.termguicolors = true


    local function my_on_attach(bufnr)
        local api = require "nvim-tree.api"

        local function opts(desc)
            return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
        end

        -- default mappings
        api.config.mappings.default_on_attach(bufnr)

        -- custom mappings
        vim.keymap.set('n', '?',     api.tree.toggle_help,                  opts('Help'))
        vim.keymap.set('n', 'w',     function() vim.cmd("wincmd p") end,    opts('Focus File'))
    end

    require("nvim-tree").setup({
        sort_by = "case_sensitive",
        renderer = {
            group_empty = true,
        },
        filters = {
            dotfiles = true,
        },
        on_attach = my_on_attach,
    })
    -- workaround to close nvim-tree if it is the last buffer
    function CloseSidePanelsIfOnly()
        local ftypes_to_check = {"NvimTree", "vista"}

        local windows = vim.api.nvim_tabpage_list_wins(0)
        for _, win in ipairs(windows) do
            local buf = vim.api.nvim_win_get_buf(win)
            local ft = vim.api.nvim_buf_get_option(buf, "filetype")
            if not vim.tbl_contains(ftypes_to_check, ft) then
                return
            end
        end

        vim.cmd("qa")
    end
    vim.api.nvim_create_autocmd("BufEnter", {
        group = vim.api.nvim_create_augroup("CloseSidePanels", {clear=true}),
        pattern = "*",
        callback = CloseSidePanelsIfOnly,
    })


    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }
    local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
    parser_config.fortran = {
        install_info = {
            url = "~/tmp/tree-sitter-fortran", -- local path or git repo
            files = {"src/parser.c", "src/scanner.c"},
            -- optional entries:
            branch = "master", -- default branch in case of git repo if different from master
            generate_requires_npm = false, -- if stand-alone parser without npm dependencies
            requires_generate_from_grammar = false, -- if folder contains pre-generated src/parser.c
        },
        filetype = "fortran", -- if filetype does not match the parser name
    }
    require'nvim-treesitter.configs'.setup({
        -- A list of parser names, or "all" (the five listed parsers should always be installed)
        ensure_installed = { "c", "lua", "vim", "vimdoc", "query", "python", "rust" , "cpp"},

        -- Install parsers synchronously (only applied to `ensure_installed`)
        sync_install = false,

        -- Automatically install missing parsers when entering buffer
        -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
        auto_install = false,

        highlight = {
            enable = true,

            -- disable slow treesitter highlight for large files
            disable = function(lang, buf)
                local max_filesize = 100 * 1024 -- 100 KB
                local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
                if ok and stats and stats.size > max_filesize then
                    return true
                end
            end,
            -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
            additional_vim_regex_highlighting = false,
        },
        incremental_selection = {
            enable = true,
            keymaps = {
                init_selection = "gnn",
                node_incremental = "grn",
                scope_incremental = "grc",
                node_decremental = "grm",
            },
        },
        indent = {
            enable = true,
            disable = {"fortran"},
        }
    })

    use 'liuchengxu/vista.vim'

    use 'Vonr/align.nvim'

    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if packer_bootstrap then
        require('packer').sync()
    end

    -- Seamless navigation between tmux panes and vim splits
    use 'christoomey/vim-tmux-navigator'

end)
