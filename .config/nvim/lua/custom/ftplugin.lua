vim.g.fortran_free_source       = true
vim.g.fortran_do_enddo          = true
vim.g.fortran_fold              = true
vim.g.fortran_fold_conditionals = true
