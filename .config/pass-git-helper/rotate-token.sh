#!/bin/bash
set -euo pipefail

usage() {
    echo "Usage: $0 <hostname>"
    echo "Example: $0 gitlab.example.com"
    exit 1
}

if [ "$#" -ne 1 ]; then
    echo "Error: Missing required hostname argument." >&2
    usage
fi

read_current_token() {
    local pass_entry=$1
    pass show "$pass_entry" | head -n 1
}

rotate_token_api_call() {
    local api_url=$1
    local token=$2

    local response http_status body

    response=$(curl --silent --write-out "HTTPSTATUS:%{http_code}" --request POST \
        --header "PRIVATE-TOKEN: $token" \
        "$api_url/personal_access_tokens/self/rotate" \
        --data "expires_at=$(date --date 'next year')")

    http_status=${response##*HTTPSTATUS:}
    body=${response%HTTPSTATUS:*}

    if [ "$http_status" -ne 200 ]; then
        echo "Error: API call failed with status $http_status." >&2
        echo "Response body: $body" >&2
        exit 1
    fi
    echo "$body"
}

extract_new_token() {
    local response=$1
    local token expires_at

    token=$(echo "$response" | jq -r '.token // empty')
    expires_at=$(echo "$response" | jq -r '.expires_at // empty')

    if [ -z "$token" ] || [ -z "$expires_at" ]; then
        echo "Error: Failed to parse token or expiry date from the response." >&2
        exit 1
    fi
    echo "$token $expires_at"
}

update_pass_entry() {
    local pass_entry=$1
    local new_token=$2

    local current_pass_entry new_pass_entry

    current_pass_entry=$(pass show "$pass_entry" | sed '1d')
    new_pass_entry="$new_token\n$current_pass_entry"

    echo -e "$new_pass_entry" | pass insert -m --force "$pass_entry"
}

rotate_gitlab_token() {
    local hostname=$1
    local pass_entry="git-token/$hostname"
    local api_url="https://$hostname/api/v4"

    local current_token response new_token_and_expiry new_token token_expires

    current_token=$(read_current_token "$pass_entry")
    response=$(rotate_token_api_call "$api_url" "$current_token")
    new_token_and_expiry=$(extract_new_token "$response")
    new_token=$(echo "$new_token_and_expiry" | awk '{print $1}')
    token_expires=$(echo "$new_token_and_expiry" | awk '{print $2}')

    if [ "$new_token" != "null" ] && [ -n "$new_token" ]; then
        update_pass_entry "$pass_entry" "$new_token"
        echo "The token for $hostname has been successfully rotated and is valid until $token_expires."
    else
        echo "Error: The token could not be rotated." >&2
        echo -e "Response from the API:\n$response" >&2
        exit 1
    fi
}

rotate_gitlab_token "$1"
