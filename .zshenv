export ZDOTDIR=~/.config/zsh/

export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1

export QT_QPA_PLATFORM=wayland-egl
export QT_QPA_PLATFORMTHEME=qt5ct

export PATH=${PATH}:/usr/bin/
export PATH=${PATH}:~/.local/bin/
export PATH=${PATH}:~/.texlive/bin/x86_64-linux
export PATH=${PATH}:~/.yarn/bin:~/.config/yarn/global/node_modules/.bin
export PATH=/opt/flutter/bin:${PATH}

export XDG_SESSION_TYPE=wayland
export XDG_CURRENT_DESKTOP=sway
    # necessary for screensharing

export KRB5CCNAME=/tmp/krb5cc_1000

export SSH_AUTH_SOCK=${XDG_RUNTIME_DIR}/ssh-agent.socket

if [[ "$(cat /etc/hostname)" == "iek8610" ]]; then
    EMAIL=$(cat ~/cloud/config/email.work)
else
    EMAIL=$(cat ~/cloud/config/email.private)
fi

git config --global user.email "$EMAIL"
git config --global user.name "$(cat ~/cloud/config/name)"
