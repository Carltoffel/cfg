#!/bin/bash

# Zustandsvariable, um zu wissen, wann wir uns im import-Block befinden.
inside_import_block=false

while IFS= read -r line || [[ -n "$line" ]]; do
    if [[ "$line" =~ ^import\ =\ \[ ]]; then
        inside_import_block=true
        echo "$line"
        continue
    fi

    if [[ "$inside_import_block" = true ]]; then
        if [[ "$line" =~ \]\$ ]]; then
            inside_import_block=false
            echo "$line"
            continue
        fi

        # Prüfe, ob die Zeile ein 'Light'- oder 'Dark'-Thema beinhaltet
        if [[ "$line" =~ Light\.toml ]]; then
            echo "${line/#\#/}"  # Entferne ein führendes `# `, falls vorhanden
        elif [[ "$line" =~ Dark\.toml ]]; then
            echo "#${line/#\#/}"  # Füge ein `# ` hinzu, falls nicht vorhanden
        else
            echo "$line"  # Keine Änderung für Zeilen, die nicht Light oder Dark sind
        fi
    else
        echo "$line"
    fi
done
